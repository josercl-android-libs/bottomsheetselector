package com.example.app

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import josercl.android.library.bottomsheetseletor.BottomSheetSelector

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn = findViewById<Button>(R.id.btn)

        val personSelector = BottomSheetSelector.Builder<Person>(this)
            .items(listOf(
                Person(1, "Jose", "josercl@gmail.com"),
                Person(2, "Fabiola", "fabiolacampero42@gmail.com"),
                Person(3, "Otra Persona", "otro@correo.com")
            ))
            .sameItemFunction { t1, t2 -> t1.id == t2.id }
            .sameContentFunction { t1, t2 -> t1 == t2 }
            .itemToStringFunction { "${it.name} - ${it.email}" }
            .listener{ btn.text = it.name }
            .build()

        btn.setOnClickListener {
            personSelector.submitList(
                listOf(
                    Person(1, "Jose Rafael", "josercl@gmail.com"),
                    Person(2, "Fabiola Campero", "fabiolacampero42@gmail.com"),
                    Person(3, "Otra Persona", "otro@correo.com")
                )
            )
            personSelector.show(supportFragmentManager, null)
        }

        val btn2 = findViewById<Button>(R.id.btn2)
        val stringSelector = BottomSheetSelector.Builder<String>(this)
            .items((1..20).map { "Opcion $it" })
            .listener { btn2.text = it }
            .build()
        btn2.setOnClickListener { stringSelector.show(supportFragmentManager, null) }
    }
}

data class Person(
    val id: Int,
    val name: String,
    val email: String
)