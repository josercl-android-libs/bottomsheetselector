## Agrega repo en build.gradle
---

    repositories {
        maven {
            url 'https://dl.bintray.com/josercl/maven'
        }
    }


## Agrega dependencias
---

    implementation 'josercl.android.library:bottomsheetselector:1.0.2'
    implementation 'com.google.android.material:material:x.y.z'
    implementation 'androidx.appcompat:appcompat:x.y.z'


## Para usar:
---

    val selector = BottomSheetSelector.Builder<U>()
        //llamado a cualquiera de los métodos
        //explicados a continuación
        .build()

    boton.setOnClickListener {
        selector.show(supportFragmentManager, null)
    }

El builder tiene los siguientes métodos, todos son opcionales:

* `items(list: List<U>)`: los elementos que se mostrarán en el selector

* `itemToStringFunction(f: (u: U) -> String)`: La función usada para mostrar las instancias de la clase U en el listado
 
* `itemIconFunction(@DrawableRes f: (u: U) -> Int)`: La función usada para mostrar un icono para cada elemento de la lista

* `itemIconTint(@ColorInt color: Int)`: El tinte de los iconos de los elementos

* `sameItemFunction(f: (t1: U, t2: U) -> Boolean)`: especifica la función usada en diffUtil (areItemsTheSame)

* `sameContentFunction(f: (t1: U, t2: U) -> Boolean)`: especifica la función usada en diffUtil (areContentsTheSame)

* `cancelText(text: String)  
cancelText(stringRes: Int)`: cambia el texto mostrado en la opción de cancelar/cerrar el selector

* `cancelIcon(@DrawableRes icon: Int)`: cambia el ícono que se muestra en la opción de cancelar/cerrar

* `cancelIconTint(@ColorInt color: Int)`: cambia el tinte que se especifica al ícono de cerrar

* `hideCancelIcon()`: ¯\\_(ツ)_/¯

* `showDivider(show: Boolean)`: especifica si se muestra una linea divisora entre los elementos de la lista

* `listener(listener: OnOptionSelectedListener)`: para especificar la acción que se ejecuta cuando se elija a cualquier elemento de la lista

Los elementos de la lista pueden ser actualizados con el método `submitList` del `BottomSheetSelector`

## Ejemplo:
---

Supongamos que queremos mostrar un listado de personas y que eso cambie el texto de un botón en una actividad

    data class Person(val id: Int, val name: String, val email: String)
    
    class SomeActivity: AppCompatActivity {
        ...
        val personSelector = BottomSheetSelector.Builder<Person>(this)
                .items(listOf(
                    Person(1, "Person 1", "person1@email"),
                    Person(2, "Person 2", "person2@email"),
                    Person(3, "Person 3", "person3@email"),
                ))
                .sameItemFunction { t1, t2 -> t1.id == t2.id }
                .sameContentFunction { t1, t2 -> t1 == t2 }
                .stringFunction { "${it.name} - ${it.email}" }
                .listener { btn.text = it.name }
                .cancelText("Some Text")
                .cancelIcon(R.drawable.ic_my_close_icon)
                .cancelIconTint(Resources.getColor(R.color.some_color))
                .build()

        btn.setOnClickListener {
            personSelector.show(supportFragmentManager, null)
        }
        ...
    }

En el caso de hacerlo en un fragment, lo único que cambia es el FragmentManager

    btn.setOnClickListener {
        personSelector.show(requireFragmentManager(), null)
    }

Para actualizar/cambiar los elementos mostrados, basta con ejecutar

    personSelector.submitList(newList)