package josercl.android.library.bottomsheetseletor

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class OptionsAdapter private constructor() {
    class Builder<T> {
        private lateinit var items: List<T>

        private var toStringFunction: (t: T) -> String = fun(t) = if (t is String) t else t.toString()
        private var sameItemFunction: (t1: T, t2: T) -> Boolean = fun(t1,t2) : Boolean { return t1 == t2 }
        private var sameContentFunction: (t1: T, t2: T) -> Boolean = fun(t1,t2) : Boolean { return t1 == t2 }
        private var toIconFunction: ((t: T) -> Int)? = null

        @ColorInt
        private var iconTint: Int? = null

        private var layout: Int = R.layout.jlbss_item_one_line
        private var listener: OnOptionSelectedListener<T>? = null

        fun items(items: List<T>): Builder<T> {
            this.items = items
            return this
        }

        fun toStringFunction(f: (t: T) -> String): Builder<T> {
            this.toStringFunction = f
            return this
        }

        fun listener(listener: OnOptionSelectedListener<T>?): Builder<T> {
            this.listener = listener
            return this
        }

        fun itemIconFunction(f: ((t: T) -> Int)?): Builder<T> {
            this.toIconFunction = f
            return this
        }

        fun itemIconTint(@ColorInt color: Int?): Builder<T> {
            this.iconTint = color
            return this
        }

        fun sameItemFunction( f: (t1: T, t2: T) -> Boolean): Builder<T> {
            this.sameItemFunction = f
            return this
        }

        fun sameContentFunction( f: (t1: T, t2: T) -> Boolean): Builder<T> {
            this.sameContentFunction = f
            return this
        }

        fun build(): ListAdapter<T, OptionsViewHolder<T>> {
            val diffUtil = object : DiffUtil.ItemCallback<T>() {
                override fun areItemsTheSame(p0: T, p1: T): Boolean = sameItemFunction(p0, p1)

                override fun areContentsTheSame(p0: T, p1: T): Boolean = sameContentFunction(p0, p1)
            }

            return object : ListAdapter<T, OptionsViewHolder<T>>(diffUtil) {
                override fun getItemViewType(position: Int): Int = layout

                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionsViewHolder<T> {
                    return OptionsViewHolder(
                        LayoutInflater.from(parent.context).inflate(viewType, parent, false),
                        listener
                    )
                }

                override fun onBindViewHolder(holder: OptionsViewHolder<T>, position: Int) {
                    val t = getItem(position)
                    holder.bindTo(t, toStringFunction(t), toIconFunction?.invoke(t), iconTint)
                }
            }.apply {
                submitList(items)
            }
        }
    }
}

class OptionsViewHolder<T>(
    view: View,
    private var listener: OnOptionSelectedListener<T>? = null
) : RecyclerView.ViewHolder(view) {
    private val text1: TextView = itemView.findViewById(R.id.jlbss_text1)

    fun bindTo(item: T, text: String, @DrawableRes icon: Int?, @ColorInt iconTint: Int?) {
        text1.text = text
        icon?.run {
            val drawableLeft = ResourcesCompat.getDrawable(itemView.resources, this, itemView.context.theme)?.mutate()
            if (drawableLeft != null && iconTint != null) {
                DrawableCompat.setTint(drawableLeft, iconTint)
            }
            text1.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, null, null, null)
        }
        itemView.setOnClickListener {
            listener?.onOptionSelected(item)
        }
    }
}

interface OnOptionSelectedListener<U> {
    fun onOptionSelected(u: U)
}