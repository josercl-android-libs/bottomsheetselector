package josercl.android.library.bottomsheetseletor

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomSheetSelector<T> : BottomSheetDialogFragment() {
    private lateinit var toStringFunction: (t: T) -> String
    private var sameItemFunction: (t1: T, t2: T) -> Boolean = fun(t1, t2): Boolean { return t1 == t2 }
    private var sameContentFunction: (t1: T, t2: T) -> Boolean = fun(t1, t2): Boolean { return t1 == t2 }
     private var toIconFunction: ((t: T) -> Int)? = null
    @ColorInt private var itemIconTint: Int? = null

    @DrawableRes private var cancelIcon: Int = 0
    @ColorInt private var cancelIconTint: Int? = null

    private lateinit var cancelText: String
    private var showDivider: Boolean = false
    private var listener: OnOptionSelectedListener<T>? = null
    private lateinit var items: List<T>
    private lateinit var adapter: ListAdapter<T, OptionsViewHolder<T>>

    class Builder<U>(private val context: Context) {
        private var toStringFunction: (t: U) -> String = fun(t) = if (t is String) t else t.toString()
        private var sameItemFunction: ((t1: U, t2: U) -> Boolean)? = null
        private var sameContentFunction: ((t1: U, t2: U) -> Boolean)? = null
        private var itemIconFunction: ((t: U) -> Int)? = null
        @ColorInt private var itemIconTint: Int? = null
        @DrawableRes private var cancelIcon: Int = R.drawable.jlbss_ic_close
        @ColorInt private var cancelIconTint: Int? = null

        private var cancelText: String = context.getString(R.string.jlbss_default_close_text)
        private var showDivider: Boolean = false
        private var items: List<U> = emptyList()
        private var listener: OnOptionSelectedListener<U>? = null

        fun itemToStringFunction(f: (u: U) -> String): Builder<U> {
            this.toStringFunction = f
            return this
        }

        fun sameItemFunction(f: (t1: U, t2: U) -> Boolean): Builder<U> {
            this.sameItemFunction = f
            return this
        }

        fun sameContentFunction(f: (t1: U, t2: U) -> Boolean): Builder<U> {
            this.sameContentFunction = f
            return this
        }

        fun itemIconFunction(f: (u: U) -> Int): Builder<U> {
            this.itemIconFunction = f
            return this
        }

        fun itemIconTint(@ColorInt color: Int): Builder<U> {
            this.itemIconTint = color
            return this
        }

        fun cancelText(@StringRes cancelText: Int): Builder<U> = cancelText(context.getString(cancelText))

        fun cancelText(cancelText: String): Builder<U> {
            this.cancelText = cancelText
            return this
        }

        fun hideCancelIcon(): Builder<U> {
            this.cancelIcon = 0
            return this
        }

        fun cancelIcon(@DrawableRes icon: Int): Builder<U> {
            this.cancelIcon = icon
            return this
        }

        fun cancelIconTint(@ColorInt color: Int): Builder<U> {
            this.cancelIconTint = color
            return this
        }

        fun showDivider(show: Boolean): Builder<U> {
            this.showDivider = show
            return this
        }

        fun items(items: List<U>): Builder<U> {
            this.items = items
            return this
        }

        fun listener(listener: OnOptionSelectedListener<U>?): Builder<U> {
            this.listener = listener
            return this
        }

        fun listener(f: (u: U) -> Unit): Builder<U> = this.listener(object :
            OnOptionSelectedListener<U> {
            override fun onOptionSelected(u: U) = f(u)
        })

        fun build(): BottomSheetSelector<U> {
            return BottomSheetSelector<U>().apply {
                toStringFunction = this@Builder.toStringFunction
                this@Builder.sameItemFunction?.run {
                    sameItemFunction = this
                }
                this@Builder.sameContentFunction?.run {
                    sameContentFunction = this
                }
                this@Builder.itemIconFunction?.run {
                    toIconFunction = this
                }
                itemIconTint = this@Builder.itemIconTint
                cancelText = this@Builder.cancelText
                showDivider = this@Builder.showDivider
                items = this@Builder.items
                cancelIcon = this@Builder.cancelIcon
                cancelIconTint = this@Builder.cancelIconTint
                listener = this@Builder.listener
            }.apply {
                adapter = OptionsAdapter.Builder<U>()
                    .items(items)
                    .sameItemFunction(sameItemFunction)
                    .sameContentFunction(sameContentFunction)
                    .toStringFunction(toStringFunction)
                    .itemIconFunction(toIconFunction)
                    .itemIconTint(itemIconTint)
                    .listener(object : OnOptionSelectedListener<U> {
                        override fun onOptionSelected(u: U) {
                            listener?.onOptionSelected(u)
                            dismiss()
                        }
                    })
                    .build()
            }
        }
    }

    fun submitList(list: List<T>) = adapter.submitList(list)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.jlbss_dialog_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val context = requireContext()

        val cancelButton: TextView = view.findViewById(R.id.cancel_button)
        val optionsList: RecyclerView = view.findViewById(R.id.jlbss_options_list)

        cancelButton.text = cancelText
        cancelButton.setOnClickListener { dismiss() }
        if (cancelIcon != 0) {
            val cancelDrawable = ResourcesCompat.getDrawable(context.resources, cancelIcon, context.theme)?.mutate()
            if (cancelDrawable != null && cancelIconTint != null) {
                DrawableCompat.setTint(cancelDrawable, cancelIconTint!!)
            }
            cancelButton.setCompoundDrawablesWithIntrinsicBounds(cancelDrawable, null, null, null)
        }

        optionsList.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        if (showDivider) {
            optionsList.addItemDecoration(DividerItemDecoration(requireContext(), RecyclerView.VERTICAL))
        }

        optionsList.adapter = adapter
    }
}